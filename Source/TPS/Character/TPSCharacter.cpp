// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "Character/TPSCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "DrawDebugHelpers.h"
#include "Engine/World.h"
#include "Weapon/TPSGameInstance.h"

ATPSCharacter::ATPSCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->IsUsingAbsoluteRotation(); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->GetRelativeRotation() = FRotator(-60.f, 0.f, 0.f);
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATPSCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	if (CurrentCursor)
	{
		APlayerController* myPC = Cast<APlayerController>(GetController());
		if (myPC)
			{
				FHitResult TraceHitResult;
				myPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
				FVector CursorFV = TraceHitResult.ImpactNormal;
				FRotator CursorR = CursorFV.Rotation();
				CurrentCursor->SetWorldLocation(TraceHitResult.Location);
				CurrentCursor->SetWorldRotation(CursorR);
		}
	}

	MovementTick(DeltaSeconds);
}

void ATPSCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	if (CursorMaterial)
	{
		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
	}
}

void ATPSCharacter::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
{
	Super::SetupPlayerInputComponent(NewInputComponent);
	NewInputComponent->BindAxis(TEXT("MoveForward"), this, &ATPSCharacter::InputAxisX);
	NewInputComponent->BindAxis(TEXT("MoveRight"), this, &ATPSCharacter::InputAxisY);

	NewInputComponent->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Released, this, &ATPSCharacter::TryReloadWeapon);

	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &ATPSCharacter::InputAttackPressed);
	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this, &ATPSCharacter::InputAttackReleased);
}

void ATPSCharacter::InputAxisY(float Value)
{
	AxisY = Value; //determinate the variable for inputs 
}

void ATPSCharacter::InputAxisX(float Value)
{
	AxisX = Value; //determinate the variable for inputs
}

void ATPSCharacter::InputAttackPressed()
{
	AttackCharEvent(true);
}

void ATPSCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}

void ATPSCharacter::MovementTick(float DeltaTime) //function to ba called in event tick
{
	APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(),0);
	if (myController)
	{
		FHitResult ResultHit;
		myController->GetHitResultUnderCursor(ECC_GameTraceChannel1, false, ResultHit);

		float YawForRotator;
		YawForRotator = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;

		SetActorRotation(FQuat(FRotator(0.0f, YawForRotator, 0.0f)));

		if (MovementState == EMovementState::SprintRun_State) 
		{
			AddMovementInput(FVector(GetActorForwardVector().X, GetActorForwardVector().Y, 0.0f), 1.0f, false);
		}
		else 
		{
			AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX, false);
			AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY, false);
			
		}
		if (CurrentWeapon)
		{
			FVector Displacement = FVector(0);
			switch (MovementState)
			{
			case EMovementState::Aim_State:
				Displacement = FVector(0.0f, 0.0f, 160.0f);
				CurrentWeapon->ShouldReduceDispersion = true;
				break;
			case EMovementState::AimWalk_State:
				CurrentWeapon->ShouldReduceDispersion = true;
				Displacement = FVector(0.0f, 0.0f, 160.0f);
				break;
			case EMovementState::Walk_State:
				Displacement = FVector(0.0f, 0.0f, 120.0f);
				CurrentWeapon->ShouldReduceDispersion = false;
				break;
			case EMovementState::Run_State:
				Displacement = FVector(0.0f, 0.0f, 120.0f);
				CurrentWeapon->ShouldReduceDispersion = false;
				break;
			case EMovementState::SprintRun_State:
				break;
			default:
				break;
			}

			CurrentWeapon->ShootEndLocation = ResultHit.Location + Displacement;
		}
	}
}

void ATPSCharacter::AttackCharEvent(bool bIsFiring)
{
	AWeaponDefault* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		myWeapon->SetWeaponStateFire(bIsFiring);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::AttackCharEvent - CurrentWeapon -NULL"));
}

void ATPSCharacter::CharacterUpdate()
{
	float ResSpeed = 600.0f;
	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResSpeed = MovementInfo.AimSpeedNormal;
		break;
	case EMovementState::AimWalk_State:
		ResSpeed = MovementInfo.AimSpeedWalk;
		break;
	case EMovementState::Walk_State:
		ResSpeed = MovementInfo.WalkSpeedNormal;
		break;
	case EMovementState::Run_State:
		ResSpeed = MovementInfo.RunSpeedNormal;
		break;
	case EMovementState::SprintRun_State:
		ResSpeed = MovementInfo.SprintRunSpeedRun;
		break;
	default:
		break;
	}

	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void ATPSCharacter::CameraStepToCharacter()
{
	
	float CurrentArmLenght = CameraBoom->TargetArmLength;
	FCameraMovement NewFCameraMovement;
	float ResultedValue;
	ResultedValue = (CurrentArmLenght + (-1 * NewFCameraMovement.CameraStep));

	if ( ResultedValue >= NewFCameraMovement.MinDistance)
	{
	CameraBoom->TargetArmLength = ResultedValue;
	}
	else
	{
	}

	
}

void ATPSCharacter::CameraStepFromCharacter()
{
	float CurrentArmLenght = CameraBoom->TargetArmLength;
	FCameraMovement NewFCameraMovement;
	float ResultedValue;
	ResultedValue = (CurrentArmLenght + (NewFCameraMovement.CameraStep));

	if (ResultedValue <= NewFCameraMovement.MaxDistance)
	{
	CameraBoom->TargetArmLength = ResultedValue;
	}
	else
	{
	}

}

void ATPSCharacter::SwitchCombatState()
{
}

UDecalComponent* ATPSCharacter::GetCursorToWorld()
{
	return nullptr;
}

void ATPSCharacter::SetMovementState(EMovementState NewMovementState)
{
	if (NewMovementState == EMovementState::Run_State)
	{
		MovementState = EMovementState::Run_State;
	}

	if (NewMovementState == EMovementState::SprintRun_State)
	{
		if (SprintRunEnabled)
		{
			MovementState = EMovementState::SprintRun_State;
		}
	}
	
	if (NewMovementState == EMovementState::Walk_State)
	{
		if (WalkEnabled)
		{
			MovementState = EMovementState::Walk_State;
		}
	}
	
	if (NewMovementState == EMovementState::Aim_State)
	{
		if (AimEnabled)
		{
			MovementState = EMovementState::Aim_State;
		}
	}
	
	if (NewMovementState == EMovementState::AimWalk_State)
	{
		if (AimEnabled)
		{
			MovementState = EMovementState::AimWalk_State;
		}
	}
	
	//if (!WalkEnabled && !SprintRunEnabled && !AimEnabled)
	//{
	//	//MovementState = EMovementState::Run_State;
	//}
	//else
	//{
	//	if (SprintRunEnabled)
	//	{
	//		WalkEnabled = false;
	//		AimEnabled = false;
	//		if (NewMovementState == EMovementState::SprintRun_State)
	//		{
	//			MovementState = EMovementState::SprintRun_State;
	//		}
	//	}
	//	if (WalkEnabled && !SprintRunEnabled && AimEnabled)
	//	{
	//		MovementState = EMovementState::AimWalk_State;
	//	}
	//	else
	//	{
	//		if (WalkEnabled && !SprintRunEnabled && !AimEnabled)
	//		{
	//			MovementState = EMovementState::Walk_State;
	//		}
	//		else
	//		{
	//			if (!WalkEnabled && !SprintRunEnabled && AimEnabled)
	//			{
	//				MovementState = EMovementState::Aim_State;
		//		}
		//	}
	//	}
	//} //OLD SOLUTION
	
	CharacterUpdate();

	//Weapon state update
	AWeaponDefault* myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		myWeapon->UpdateStateWeapon(MovementState);
	}
}

AWeaponDefault* ATPSCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

void ATPSCharacter::InitWeapon(FName IdWeaponName)
{
	UTPSGameInstance* myGI = Cast<UTPSGameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;
	if (myGI)
	{
		UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::InitWeapon - Success Game Instance"));

		if (myGI->GetWeaponInfoByName(IdWeaponName, myWeaponInfo))
		{
			UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::InitWeapon - Success IdWeaponName"));

			if (myWeaponInfo.WeaponClass)
			{
				UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::InitWeapon - Success WeaponClass"));

				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myWeapon)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
					CurrentWeapon = myWeapon;

					myWeapon->WeaponSetting = myWeaponInfo;
					myWeapon->WeaponInfo.Round = myWeaponInfo.MaxRound;
					//Remove !!! Debug
					myWeapon->ReloadTime = myWeaponInfo.ReloadTime;
					myWeapon->UpdateStateWeapon(MovementState);

					myWeapon->OnWeaponReloadStart.AddDynamic(this, &ATPSCharacter::WeaponReloadStart);
					myWeapon->OnWeaponReloadEnd.AddDynamic(this, &ATPSCharacter::WeaponReloadEnd);
				}
			}
			
			UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::InitWeapon - Success init"));
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::InitWeapon - Weapon not found in table -NULL"));
		}
	}


}

void ATPSCharacter::TryReloadWeapon()
{
	if (CurrentWeapon)
	{
		if (CurrentWeapon->GetWeaponRound() <= CurrentWeapon->WeaponSetting.MaxRound)
		{
			CurrentWeapon->InitReload();	
			//Find animation
			if (CurrentWeapon->WeaponSetting.AnimCharReload)
			{
				UAnimMontage* NewAnimMontage;
				NewAnimMontage = CurrentWeapon->WeaponSetting.AnimCharReload;
				WeaponReloadStart(NewAnimMontage);
			}
		}
	}
}

//void ATPSCharacter::WeaponReloadStart_BP(UAnimMontage* Anim)
//{
//}

//void ATPSCharacter::WeaponReloadEnd_BP()
//{
//}

void ATPSCharacter::WeaponReloadStart(UAnimMontage* Anim)
{
//	WeaponReloadStart_BP(Anim);
	GetMesh()->PlayAnimation(Anim, false);
}

void ATPSCharacter::WeaponReloadEnd()
{
//	WeaponReloadEnd_BP();
}